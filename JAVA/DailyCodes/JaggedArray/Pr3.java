import java.util.*;

class TwoDInput{

	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);

		System.out.println("Enter the array size");

		int row = sc.nextInt();
		
		int col = sc.nextInt();

		int arr[][] = new int[row][col];

		//Insert data: Array

		System.out.println("Enter array elements : ");

		for(int i=0; i<arr.length; i++){

			for(int j=0; j<arr[i].length; j++){

				arr[i][j] = sc.nextInt();
			}

		}

		System.out.println("Array elements are : ");

		for(int i=0; i<arr.length; i++){

			for(int j=0; j<arr[i].length; j++){

				System.out.print(arr[i][j]+" ");
			}

			System.out.println();
		}
	}
}
