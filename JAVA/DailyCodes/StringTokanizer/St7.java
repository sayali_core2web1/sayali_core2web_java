import java.util.*;

class SingleLine{

	public static void main(String[] arsg){

		Scanner sc = new Scanner(System.in);

		System.out.println("Enter Player Info");

		String info = sc.nextLine();

		StringTokenizer st = new StringTokenizer(info,"@#");

		while(st.hasMoreTokens()){

			System.out.println(st.nextToken());
		}
	}
}
