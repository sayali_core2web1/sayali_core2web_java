import java.io.*;

class InputDemo{

	public static void main(String[] args)throws IOException{

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter your name");

		String name = br.readLine();

		System.out.println("Enter Society Name");

		String socName = br.readLine();

		System.out.println("Enter Wing ");

		char wing = (char)br.read();

		System.out.println("Enter flat no.");

		int flatNo = br.readLine();

		System.out.println("Name : " +name);
		System.out.println("Society name :" +socName);
		System.out.println("Wing : " +wing);
		System.out.println("Flat no. :" +flatNo);

	}
}
