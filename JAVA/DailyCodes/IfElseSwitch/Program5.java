class SwitchDemo{

	public static void main(String[] args){

		float num = 90.40f;

		switch(num){

			case 90.10:
				System.out.println("90.10");
				break;
			
			case 90.20:
				System.out.println("90.20");
				break;

			case 90.40:
				System.out.println("90.40");
				break;

			default :
				System.out.println("In Default State");
				break;
		}
	}
}
