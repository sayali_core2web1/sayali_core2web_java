class SwitchStringDemo{

	public static void main(String[] args){

		String friend = "Aarya";
		System.out.println("Before Switch");

		switch(friend){

			case "Gauri":
				System.out.println("Biotechnology");
				break;

			case "Anjali":
				System.out.println("Physiotherapy");
				break;

			case "Sanika":
				System.out.println("CA");
				break;

			case "Aarya":
				System.out.println("LLB");
				break;

			case "Jaju":
				System.out.println("Bussiness");
				break;

			default :
				System.out.println("In Default Case");
		}
		System.out.println("After Switch");
	}
}
