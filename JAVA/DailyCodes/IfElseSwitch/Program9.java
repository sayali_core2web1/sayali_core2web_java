class SwitchDemo{

	public static void main(String[] args){

		String games = "Treasure Hunt";

		switch(games){

			default :
				System.out.println("In Default Case");
				break;

			case "Test Your Bond ":
				System.out.println("Test Your Bond");
				break;

			case "Treasure Hunt":
				System.out.println("Treasure Hunt");
				break;

			case "Code fest":
				System.out.println("Code fest");
				break;
		}

	}
}
