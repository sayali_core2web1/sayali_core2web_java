class SwitchCase{

	public static void main(String[] args){

		int x = 50;

		switch(x){

			case 20+30:
				System.out.println("20+30");
				break;

			case 
			case 25+25:
				System.out.println("25+25");
				break;

			case 50:
				System.out.println("50");
				break;

			default: 
				System.out.println("In Default State");
		}
		System.out.println("End Code");
	}
}
