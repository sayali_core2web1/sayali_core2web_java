class SwitchDemo{

	public static void main(String[] args){

		float num = 15;
		System.out.println("Before Switch");

		switch(num){
		
			case 15:
				System.out.println("15");
				break;

			case 16:
				System.out.println("16");
				break;

			case 17:
				System.out.println("17");
				break;

			default :
				System.out.println("In Default case");
		}
		System.out.println("After Switch");
	}
}
