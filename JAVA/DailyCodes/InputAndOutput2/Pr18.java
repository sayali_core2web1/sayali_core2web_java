import java.io.*;

class InputDemo{

	public static void main(String[] args)throws IOException{

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.print("Enter Company Name : ");
		String cmpName = br.readLine();

		System.out.print("Enter Employee Name : ");
		String empName = br.readLine();

		System.out.print("Enter Employee ID : ");
		int empId = Integer.parseInt(br.readLine());

		System.out.print("Enter Employee Salery :");
		double empSalary = Double.parseDouble(br.readLine());

		System.out.println("Company Name is : "+cmpName);
		System.out.println("Employee Name is : "+empName);
		System.out.println("Employee Id is : "+empId);
		System.out.println("Employee Salary is :"+empSalary);
	}
}

