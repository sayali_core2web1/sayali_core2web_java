import java.util.Scanner;

class ScannerDemo{

	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);

		System.out.print("Enter Student Name : ");
		String name = sc.next();

		System.out.print("Enter Colege Name : ");
		String clgName = sc.next();

		System.out.print("Enter Student ID : ");
		int studID = sc.nextInt();

		System.out.print("Enter Student CGPA : ");
		float studCGPA = sc.nextFloat();

		System.out.println("Student name : "+name);
		System.out.println("Student College : "+clgName);
		System.out.println("Student ID : "+studID);
		System.out.println("Student CGPA : "+studCGPA);
	}
}
