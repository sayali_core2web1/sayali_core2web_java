import java.io.*;

class InputDemo{

	public static void main(String[] args)throws IOException{

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.print("Enter Company Name : ");
		String compName = br.readLine();

		System.out.print("Enter Employee Name : ");
		String empName = br.readLine();

		System.out.print("Enter Employee Id : ");
		String empId= br.readLine();

		System.out.println("Company Name is : "+compName);
		System.out.println("Employee Name is :"+empName);
		System.out.println("Employee Id is :"+empId);
	}
}
