import java.io.*;

class InputDemo{

	public static void main(String[] args)throws IOException{

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.print("Enter Company Name : ");
		String cmpName = br.readLine();

		System.out.print("Enter Employee : ");
		String empName = br.readLine();

		System.out.println("Company Name is :"+cmpName);
		System.out.println("Employee Name is : "+empName);
	}
}
