class InputDemo{

	void MethodFun(){

		System.out.println("In Method Fun");
	}
	void MethodGun(){

		System.out.println("In Method Gun");
	}
	void MethodRun(){

		System.out.println("In Method Run");
	}
	public static void main(String[] args){

		System.out.println("In Main method");
		InputDemo obj = new InputDemo();
		MethodFun();
		MethodGun();
		MethodRun();
	}
}
