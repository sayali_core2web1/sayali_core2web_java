class InputDemo{

	void MethodFun(){

		System.out.println("In Method Fun");
	}
	void MethodGun(){

		System.out.println("In Method Gun");
	}
	void MethodRun(){

		System.out.println("In Method Run");
	}
	public static void main(String[] args){

		System.out.println("In Main Method");
		InputDemo obj = new InputDemo();
		obj.MethodFun();
		obj.MethodGun();
		obj.MethodRun();
	}
}
