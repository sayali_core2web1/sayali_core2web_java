class InputDemo{

	void fun(){

		System.out.println("In Fun Method");
	}
	static void run(){

		System.out.println("In Run Method");
	}
	public static void main(String[] args){

		System.out.println("In Main method");
		run();
		InputDemo obj = new InputDemo();
		obj.fun();
	}

}
