class C2W{

	public static void main(String[] args){

		short var1 = 10;
		boolean var2 = true;
		char var3 = 'Z';
		float var4 = 10.00f;
		double var5 = 10.09D;
		long var6 = 11L;

		int num1 = var1;
		int num2 = var2;
		int num3 = var3;
		int num4 = var4;
		int num5 = var5;
		int num6 = var6;

		System.out.println(num1);
		System.out.println(num2);
		System.out.println(num3);
		System.out.println(num4);
		System.out.println(num5);
		System.out.println(num6);
	}
}
