class IfElseDemo{

	public static void main(String[] args){

		char ch = 'A';

		if(ch>='a' && ch<='z'){

			System.out.println(ch +" is a lowercase Character");
		}
		else if(ch>='A' && ch<='Z'){

			System.out.println(ch +" is an Uppercase Character");
		}
	}

}
