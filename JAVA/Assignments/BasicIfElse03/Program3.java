class IfDemo{

	public static void main(String[] args){

		int num = 10;
		if(num%2==1 && num >10){

			System.out.println(num+" is an odd number and greater than 10");
		}
		else if(num%2==0 && num<10){

			System.out.println(num+" is an even number and less than 10");
		}
		else if(num%2==0 && num ==10){

			System.out.println(num+" is an even num and equal to zero ");
		}

	}
}
