class Trip{

	public static void main(String[] args){

		int num = 15000;

		switch(num){

			case 15000:
				System.out.println("Destination in Jammu & Kashmir");
				break;

			case 10000:
				System.out.println("Destination in Manali");
				break;

			case 6000:
				System.out.println("Destination in Amritsar");
				break;

			case 2000:
				System.out.println("Destination in Mahabaleshwar");
				break;

			default:
				System.out.println("Try next time");
		}
	}
}
