class Travelling{

	public static void main(String[] args){

		int money = 50;

		switch(money){

			case 200:
				System.out.println("Book a Cab");
				break;

			case 150:
				System.out.println("Book a Auto");
				break;

			case 100:
				System.out.println("Book Pvt Vehicle");
				break;

			case 50:
				System.out.println("PMPL Bus");
				break;

			default:
				System.out.println("You are out of Money now!");
		}
	}
}
