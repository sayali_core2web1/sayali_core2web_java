class Size{

	public static void main(String[] args){

		String ch = "XL";

		switch(ch){

			case "XL":
				System.out.println("Extra Large");
				break;

			case "L":
				System.out.println("Large");
				break;

			case "M":
				System.out.println("Medium");
				break;

			case "S":
				System.out.println("Small");
				break;

			case "XS":
				System.out.println("Extra Small");
				break;

			default :
				System.out.println("Invalid Size");
		}
	}
}
