class PosNegNum{

	public static void main(String[] args){

		int num1 = 2;
		int num2 = 5;
		int mul = 0;

		if(num1>0 && num2>0){

			mul= num1*num2;
			System.out.println("Multiply of "+ num1 +" & "+ num2 +" is "+ mul);

			if(mul%2==0){

				System.out.println(mul +" is an even no.");
			}else{

				System.out.println(mul +" is an odd no.");
			}
		}
	}
}
