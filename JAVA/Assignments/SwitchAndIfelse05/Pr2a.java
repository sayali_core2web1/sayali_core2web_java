class Remark{

	public static void main(String[] args){

		char ch = 'O';

		switch(ch){

			case 'O':
				System.out.println("Outstanding");
				break;

			case 'A':
				System.out.println("Excellent");
				break;

			case 'B':
				System.out.println("Good");
				break;

			case 'C':
				System.out.println("Average");
				break;

			default:
				System.out.println("Below Average");
		}
	}
}
