class Data{

	public static void main(String[] args){

		byte Date = 26;
		int SecInDay = 86400;
		byte Month = 03;
		float SecInMonth = 2628002.88f;
		int Year = 2003;
		int SecInYear = 31536000;

		System.out.println("Date = "+Date);

		System.out.println("Seconds in a Day = "+SecInDay);

		System.out.println("Month = "+Month);

		System.out.println("Seconds in a Month = "+SecInMonth);

		System.out.println("Year = "+Year);

		System.out.println("Seconds in a Year = "+SecInYear);
	}
}
