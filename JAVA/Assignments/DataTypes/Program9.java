class Temperature{

	public static void main(String[] args){

		byte ACTemp = 26;
		byte RoomTemp = 22;

		System.out.println("Temperature of Air Conditioner is "+ACTemp +" degree Celsius");

		System.out.println("Standard Room Temperature is "+RoomTemp +" degree Celsius");
	}
}
