class Gravity{

	public static void main(String[] args){

		float GravityVal = 9.8f;
		char GravityRep = 'g';

		System.out.println("Value of gravity is "+GravityVal);

		System.out.println("Letter Representation of gravity is "+GravityRep);
	}
}
