
import java.util.*;

class TableOfTwo{

	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);

		int row = sc.nextInt();

		int num = 2;
		int temp =1;

		for(int i=row ; i>=1; i--){

			for(int j=i ; j>=1 ;j--){

				System.out.print(num*temp +" ");
				temp++;
			}
			
			System.out.println();
		}
	}


}

