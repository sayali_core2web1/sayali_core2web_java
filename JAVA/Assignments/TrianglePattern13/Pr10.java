import java.util.*;

class AlphabetRev{

	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);

		System.out.print("rows = ");

		int row = sc.nextInt();

		int ch1 = 64+row;

		int ch2 = 96+row;

		for(int i=row; i>=1; i--){

			for(int j=i; j>=1; j--){

				if(i%2==1){

					System.out.print((char)ch2 +" ");

				}else{

					System.out.print((char)ch1 +" ");
				}
				ch2--;
				ch1--;
			}
			ch1= 64+i-1;
			ch2= 96+i-1;
			System.out.println();
		}
	}
}
