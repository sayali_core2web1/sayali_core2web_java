import java.util.*;

class AlphaNumeric{

	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);

		System.out.print("rows = ");

		int row = sc.nextInt();

		int num = row;

		int ch = 64+row;

		int temp = 1;

		for(int i=row; i>=1; i--){

			for(int j=i; j>=1; j--){

				if(temp%2 == 1){

					System.out.print(num +" ");
				}else{

					System.out.print((char)ch +" ");
				}
				num--;
				ch--;
			}
			temp++;
			num = i-1;
			ch = 64+i-1;
			System.out.println();
		}
	}
}
