import java.util.*;

class Numbers{

	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);

		int row = sc.nextInt();

		int num = 1;

		for(int i=row ; i>=1; i--){

			for(int j=i ; j>=1 ;j--){

				System.out.print(num +" ");
				num++;
			}
			num = num-(i-1);
			System.out.println();
		}
	}


}
