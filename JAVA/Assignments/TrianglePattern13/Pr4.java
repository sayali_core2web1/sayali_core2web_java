import java.util.Scanner;

class RevAlphabet{

	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);

		System.out.print("rows = ");

		int row = sc.nextInt();

		int ch = 64+row*2;

		for(int i=row; i>=1; i--){

			for(int j=i; j>=1; j--){

				System.out.print((char)ch +" ");
				ch--;
			}
			System.out.println();
		}
	}
}
