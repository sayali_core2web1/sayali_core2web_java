import java.util.*;

class OddRev{

	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);

		System.out.print("rows = ");

		int row = sc.nextInt();

		int num = (row*(row+1))-1;

		for(int i=row*2; i>=1; i--){

			for(int j=i*2; j>=1; j--) {

				if(num%2==1){

					System.out.print(num +" ");
				}
			}
			System.out.println();
		}
	}
}
