import java.util.*;

class AlphaNumeric{

	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);

		int row = sc.nextInt();

		for(int i=row; i>=1 ; i--){

			int num = 1;
			int ch = 97;
			int temp = 1;

			for(int j=i ; j>=1 ; j--){

				if(temp%2==0){

			        	System.out.print((char)ch +" ");
					ch++;
					
				}else{

					System.out.print(num +" ");
					num++;
				}
				
					temp++;
			}
			System.out.println();
		}
	}
}
