import java.util.*;

class ReverseAlphaNum{

	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);

		System.out.print("rows = ");

		int row = sc.nextInt();
		int num = row;
		int ch = 96+row;

		for(int i=row; i>=1; i--){

			for(int j=i; j>=1; j--){

				if(i%2==0){

					if(num%2==0){
						
						System.out.print(num +" ");

					}else{

						System.out.print((char)ch +" ");
					}
					num--;
					ch--;

				}else{

					if(num%2==1){

						System.out.print(num +" ");
					}else{

						System.out.print((char)ch +" ");
					}
					num--;
					ch--;
				}
			}
			num = i-1;
			ch = 96+i-1;
			System.out.println();

		}
	}
}
