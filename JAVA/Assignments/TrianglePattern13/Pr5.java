import java.util.*;

class Alphabets{

	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);

		int row = sc.nextInt();

		for(int i=row; i>=1 ; i--){

			int ch1 = 65;
			int ch2 = 97;

			for(int j=i ; j>=1 ; j--){

				if(i%2==0){

					System.out.print((char)ch1 +" ");
					ch1++;
				}else{

					System.out.print((char)ch2 +" ");
					ch2++;
				}
			}
			System.out.println();
		}
	}
}
