import java.util.*;

class FirstOccurance{

	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);

		System.out.print("Size : ");

		int size = sc.nextInt();

		int arr[] = new int[size];

		System.out.print("Enter array elements : ");

		for(int i=0; i<arr.length; i++){

			arr[i] = sc.nextInt();
		}
		System.out.print("Specific number : ");
		int num = sc.nextInt();

		for(int i=0; i<arr.length; i++){

			if(arr[i]==num){

				System.out.println("num "+ num +" first occured at index "+ i);
			}else{
			
				System.out.println("num "+ num +" not found in array");

			}
		}
	//	System.out.println("num "+ num +" not found in array");
		
	}
}
