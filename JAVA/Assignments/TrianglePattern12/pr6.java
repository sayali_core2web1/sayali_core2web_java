import java.io.*;

class AlphaNumeric{

	public static void main(String[] args)throws IOException{

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		int row = Integer.parseInt(br.readLine());

		int ch = 65;

		for(int i = 1; i<=row; i++){

			int num = 1;

			for(int j=1; j<=i; j++){

				if(i%2==0){
					
					System.out.print((char)ch +" ");

				}else{
					
					System.out.print(num++ +" ");
				}
				ch++;
			}
			System.out.println();
		}
	}
}
	
