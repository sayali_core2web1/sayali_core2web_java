import java.io.*;

class Alphabet{

	public static void main(String[] args)throws IOException{

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		int row = Integer.parseInt(br.readLine());

		for(int i= 1; i<=row; i++){

			int ch = row+64;

			for(int j= 1; j<=i; j++){

				System.out.print((char)ch-- +" ");
			}
			System.out.println();
		}

	}
}
