import java.util.*;

class AlphaNumeric{

	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);

		int row = sc.nextInt();
		int ch = 97;

		for(int i=1; i<=row; i++){

			int num = 1;
			for(int j=1; j<=i ; j++){

				if(j%2==1){

					System.out.print(num +" ");
					
				}else{

					System.out.print((char)ch +" ");
				}
				num++;
				ch+=1;
			}
			System.out.println();
		}
	}

}
