import java.util.Scanner;

class AlphaNumeric{

	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);

		int row = sc.nextInt();
		int ch = 97;

		for(int i=1; i<=row; i++){

			int num = row+1;

			for(int j=1; j<=i ;j++){

				if(j%2==0){

					System.out.print((char)ch +" ");
					ch++;
				}else{

					System.out.print(num +" ");
				}
				num++;
			}
			System.out.println();
		}
	}
}
