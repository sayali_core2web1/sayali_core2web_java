import java.io.*;

class Alphabet{

	public static void main(String[] args)throws IOException{

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		int row = Integer.parseInt(br.readLine());
		
		for(int i=1; i<=row; i++){
		
			int lch = row+96;
			int uch = row+64;

			for(int j= 1; j<=i; j++){

				if(i%2==1){

					System.out.print((char)lch-- +" ");
				}else{

					System.out.print((char)uch-- +" ");
				}
			}
			System.out.println();

		
		}


	}
}
