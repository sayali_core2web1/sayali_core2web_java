import  java.io.*;

class Symbol{

	public static void main(String[] args)throws IOException{

		BufferedReader br = new BufferedReader (new InputStreamReader(System.in));

		int row = Integer.parseInt(br.readLine());

		for(int i= 1; i<=row; i++){

			char ch = 97;

			for(int j=1; j<=i; j++){

				if(i%2==1){

					System.out.print(ch++ +" ");
				}else{

					System.out.print("$ ");
				}
			}
			System.out.println();
		}
	}
}
