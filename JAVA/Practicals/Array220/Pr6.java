import java.util.Scanner;

class ProductOfOdd{

	public static void main(String[] sayali){

		Scanner sc = new Scanner(System.in);

		System.out.println("Enter array size:");

		int size = sc.nextInt();

		int arr[] = new int[size];

		System.out.println("Enter array elements:");

		for(int i=0; i<arr.length ; i++){

			arr[i] = sc.nextInt();
		}
		int pro = 1;

		for(int i=0; i<arr.length; i++){

			if(i%2==1){

				pro = pro*arr[i];
			}
		}
		System.out.println("product of odd indexed element is : "+pro);

	}
}
