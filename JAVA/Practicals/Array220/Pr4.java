import java.util.*;

class SearchElement{

	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);

		System.out.println("Enter array size");

		int size = sc.nextInt();

		int arr[] = new int[size];

		System.out.println("Enter elements:");

		for(int i=0; i<arr.length; i++){

			arr[i]= sc.nextInt();
		}
		System.out.println("Enter the number to search in array:");

		int num = sc.nextInt();

		for(int i=0; i<arr.length; i++){

			if(arr[i]==num){

				System.out.println(num +" found at index "+ i);
			}

		}
	}
}
