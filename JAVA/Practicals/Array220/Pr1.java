import java.util.*;

class EvenNumbers{

	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);

		System.out.print("Enter array size : ");

		int size = sc.nextInt();

		int arr[] = new int[size];

		System.out.print("Enter array elements : ");

		for(int i=0; i<arr.length; i++){

			arr[i] = sc.nextInt(); 
		}
		System.out.print("Even elements in an array :");
		
		int count = 0;
		for(int i=0; i<arr.length; i++){

			if(arr[i]%2==0){

				System.out.print(" "+ arr[i] +" ");
				count++;
			}
		}
		System.out.println();
		System.out.println("Count of even elemrnts : "+ count);
	}
}
