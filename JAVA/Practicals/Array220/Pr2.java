import java.util.*;

class DivisibleBy3{

	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);

		System.out.print("Enter array size : ");

		int size = sc.nextInt();

		int arr[] = new int[size];

		System.out.print("Array elements are : ");

		for(int i=0; i<arr.length; i++){

			arr[i] = sc.nextInt();
		}
		int sum = 0;

		System.out.print("Elements divisible by 3 : ");

		for(int i=0; i<arr.length ; i++){

			if(arr[i] %3==0){

				System.out.print(" "+arr[i]+" ");
				sum = sum + arr[i];
			}
		}
		System.out.println();
		System.out.println("Sum of elements divisible by 3 is "+sum);
	}
}
