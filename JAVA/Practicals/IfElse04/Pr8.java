class Resut{

	public static void main(String[] args){

		float percent = 35.00f;

		if(percent>=70.00){

			System.out.println("Passed : First class with distinction");
		
		}else if(percent<70.00 && percent>=60.00){

			System.out.println("Passed : First class");

		}else if(percent<60.00 && percent >=50.00){

			System.out.println("Passed : Second class");
		
		}else if(percent<50.00 && percent>=40.00){


			System.out.println("Passed : Third class");

		}else {

			System.out.println("Fail");
		}

	}

}

