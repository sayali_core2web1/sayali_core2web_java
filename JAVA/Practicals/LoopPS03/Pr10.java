class NestedLoop{

	public static void main(String[] args){

		int num = 1;
		int i=1;
		while(i<=3){

			for(int j=1; j<=3; j++){

				System.out.print(num++ +" ");
			}
			System.out.println();
			i++;
		}
	}
}
