
import java.util.*;

class Numbers{

	public static void main(String[] arsg){

		Scanner sc = new Scanner(System.in);

		int row = sc.nextInt();
		int num = 1;

		for(int i=1; i<=row; i++){

			for(int sp = 1; sp<i ; sp++){

				System.out.print("  ");
			}

			for(int j=row; j>=i ; j--){

				System.out.print(num +" ");
				num++;
			}
		        num=i+1;
			System.out.println();
		}
	}
}
