import java.util.*;

class TableOfRow{

	public static void main(String[] arsg){

		Scanner sc = new Scanner(System.in);

		int row = sc.nextInt();

		for(int i=1; i<=row; i++){

			for(int sp = 1; sp<=row-i ; sp++){

				System.out.print("  ");
			}

			int num = row;
			for(int j=1; j<=i ; j++){

				System.out.print(num*j  +" ");
			}
		
			System.out.println();
		}
	}
}
