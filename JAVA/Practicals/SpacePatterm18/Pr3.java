import java.util.*;

class AlphabetReverse{

	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);

		int row = sc.nextInt();

		int ch=64+row;
		for(int i=1; i<=row; i++){

			for(int sp=1; sp<=row-i ; sp++){

				System.out.print("  ");
			}
			
		        
			for(int j=1; j<=i ; j++){

				System.out.print((char)ch +" ");
				ch++;
			}
			ch-=i+1;
		
			System.out.println();
		}

		
	}
}
