
import java.util.*;

class Alphabets{

	public static void main(String[] arsg){

		Scanner sc = new Scanner(System.in);

		int row = sc.nextInt();

		for(int i=1; i<=row; i++){

			for(int sp = 1; sp<i ; sp++){

				System.out.print("  ");
			}

		        int ch = 64+row;
			for(int j=row; j>=i ; j--){

				System.out.print((char)ch +" ");
				ch--;
			}
		
			System.out.println();
		}
	}
}
