import java.io.*;

class ReverseNumber{

	public static void main(String[] args)throws IOException{

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		int num = Integer.parseInt(br.readLine());

		int temp = num ;

		int rev = 0;

		int rem = 0;

		while(temp>0){

			rem = temp%10;

			rev = rev*10 + rem;

			temp = temp/10;
		}
		System.out.println("Reverse of "+ num +" is "+ rev);
	}


}
