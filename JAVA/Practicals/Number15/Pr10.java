import java.util.Scanner;

class PalindromeNumber{

	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);

		int num = sc.nextInt();

		int temp = num;

		int rem = 0;

		int rev = 0;

		while(temp>0){

			rem = temp%10;
			rev = rev*10 + rem;
			temp = temp/10;
		}
		if(num==rev){

			System.out.println(num +" is a Palindrome Number" );
		}else{

			System.out.println(num +" is not a Palindrome Number");
		}
	}
}


