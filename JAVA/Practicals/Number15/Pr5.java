import java.io.*;

class Factorial{

	public static void main(String[] args)throws IOException{

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		int num = Integer.parseInt(br.readLine());

		int temp = num;

		int fact = 1;

		while(temp>0){

			fact = temp*fact;
			temp--;
		}
		System.out.println("Factorial of "+ num +" is "+ fact);
	}
}
