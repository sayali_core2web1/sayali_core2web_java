import java.io.*;

class Factors{

	public static void main(String[] args)throws IOException{

		BufferedReader br = new BufferedReader (new InputStreamReader(System.in));

		int num = Integer.parseInt(br.readLine());

		int temp = 1;

		int fact = 0;

		System.out.print("Factors of "+ num +" are ");

		while(temp<=num){

			if(num%temp == 0){
				if(temp<12){
				
					System.out.print(temp+",");
				}else{

					System.out.print(temp);
				}
			}
			temp++;

		}
		//System.out.print("Factorial of "+ num +" are "+ fact);
		System.out.println();

	}

}
