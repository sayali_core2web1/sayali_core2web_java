import java.util.Scanner;

class AlphaNumeric{

	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);

		int row = sc.nextInt();

		for(int i=1; i<=row; i++){

			int num = 1;
			int ch = 64+row;

			for(int j=1; j<=row; j++){

				if(i%2==0){

					System.out.print(" "+ num +" ");
					num++;
				}else{

					System.out.print(" "+ (char)ch +" ");
					ch--;
				}
			}
			System.out.println();
		}
	}
}
