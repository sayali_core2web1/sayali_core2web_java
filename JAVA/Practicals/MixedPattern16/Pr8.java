/*J I H G
  F E D
  C B
  A*/

import java.util.*;

class RevAlphabet{

	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);

		int row = sc.nextInt();

		int temp = 0;
		int ch = 64+(row);

		for(int i=row; i>=1 ; i--){

			for(int j=i ; j>=1 ; j--){

				System.out.print((char)ch +" ");
				ch--;
			}
			temp++;
			System.out.println();
		}
	}
}

