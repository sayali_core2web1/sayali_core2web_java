import java.util.*;

class SquareOfOdds{

	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);

		long num = sc.nextLong();

		long rev = 0;

		long temp = num;

		long rem = 0;

		while(temp>0){

			rem = temp%10;
			rev = rev*10+rem;
			temp = temp/10;
			
			if(rem%2==1){

				System.out.print(rem*rem +",");
			}
		}
		System.out.println();
	}
}
