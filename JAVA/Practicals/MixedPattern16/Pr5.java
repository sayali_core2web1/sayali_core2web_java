import java.util.*;

class Numbers{

	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);

		int row = sc.nextInt();

		int num= 1;
		int mul = 0;

		for(int i=1 ; i<=row ; i++){

			for(int j=1; j<=i ; j++){

				mul = num*j;
				System.out.print(mul +" ");
			}
			num++;
			System.out.println();
		}

	}
}
