import java.util.Scanner;

class AlphaNumeric{

	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);

		int row = sc.nextInt();

		for(int i=1; i<=row ;i++){

			int num = row;
			int ch = 96+row;

			for(int j=1; j<=i; j++){

				if(i%2==1){

					System.out.print((char)ch +" ");
					ch--;
				}else{

					System.out.print(num +" ");
					num--;
				}
			}
			System.out.println();
		}
	}
}
