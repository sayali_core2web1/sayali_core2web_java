import java.util.*;

class Alphabet{

	public static void main(String[] args){

		Scanner sc = new Scanner (System.in);

		int row = sc.nextInt();

		for(int i=row ; i>=1 ; i--){

			int ch = row+64;

			for(int j=i ; j>=1 ; j--){

				System.out.print((char)ch +" ");
				ch--;
			}
			System.out.println();
		}
	}
}
