import java.util.*;

class NumPattern{

	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);

		int row = sc.nextInt();

		for(int i=row ; i>=1 ; i--){

			int num = 1;

			for(int j=i ; j>=1 ; j--){

				System.out.print(num +" ");
				num++;
			}
			System.out.println();
		}

	}
}
