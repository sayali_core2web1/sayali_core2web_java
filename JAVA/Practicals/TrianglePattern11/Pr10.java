import java.util.Scanner;

class AlphaNumeric{

	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);

		int row = sc.nextInt();
		int num = 65;

		for(int i=row; i>=1 ;i--){

			for(int j=i ; j>=1 ; j--){

				if(j%2==0){

					System.out.print(num +" ");
				}else{

					System.out.print((char)num +" " );
				}
				num++;
			}
			num = num-(i-1);
			System.out.println();
		}
	}
}
