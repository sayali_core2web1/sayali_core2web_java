class CountOandE{

	public static void main(String[] args){

		long num = 214367689l;
		int countEven=0;
		int countOdd=0;
		long rem = 0l;

		while(num>0){

			rem = num%10;
			if(rem%2 == 0){

				countEven++;
			}else{

				countOdd++;
			}
			num = num/10;
			
		}
		System.out.println("Even Count = "+countEven);
		System.out.println("Odd Count = "+countOdd);

	}
}
