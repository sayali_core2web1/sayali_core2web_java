class DigitSum{

	public static void main(String[] args){

		long num = 9307922405l;
		long sum = 0l;
		long rem = 0l;
	

		while(num>0){

			rem = num%10;
			num = num/10;
			sum = sum + rem;
		}
		System.out.println("Sum of digits in 930792205 is "+sum);
	}
}
