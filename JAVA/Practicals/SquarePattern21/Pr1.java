import java.util.*;

class AlphaNumeric{

	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);
		
		System.out.print("row = ");
		int row = sc.nextInt();
		
		int num = row;

		for(int i=1 ; i<=row ; i++){

			int ch = 64+row;

			for(int j=1 ; j<=row; j++){

				if(i%2==0){

					System.out.print(num +" ");
				}else{

					System.out.print((char)ch +" ");
					ch--;
				}

			}
			System.out.println();
		}
	}

}
