import java.util.*;

class Combination{

	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);

		System.out.print("rows = ");

		int row = sc.nextInt();

		for(int i=1; i<=row; i++){

			int ch = 64+row;

			for(int j=1; j<=row; j++){

				if(i%2==1){

					if(j%2==1){

						System.out.print("# ");
					}else{

						System.out.print((char)ch +" ");
						ch--;
					}

				}else{

					if(j%2==0){

						System.out.print("# ");
					}else{

						System.out.print((char)ch +" ");
						ch--;
					}
					
				}
			}
			System.out.println();
		}
	}
}
