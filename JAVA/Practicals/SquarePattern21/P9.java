import java.util.*;

class MultiplyBy2and3{

	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);

		System.out.print("rows = ");

		int row = sc.nextInt();

		for(int i=1; i<=row; i++){

			int num = 1;

			for(int j=1; j<=row; j++){

				if(i%2==1){
					if(num%2==1){
					
						System.out.print(num*2 +" ");
		
					}else{
			
						System.out.print(num*3 +" ");
			
					}
					num++;
				}else{

					if(num%2==1){

						System.out.print(num*3 +" ");
					}else{

						System.out.print(num*2 +" ");
					}
					num++;
				}
			}
			System.out.println();
		}
	}
}
