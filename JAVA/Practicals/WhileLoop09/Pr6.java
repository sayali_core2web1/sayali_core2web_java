class ProductOfDigit{

	public static void main(String[] args){

		int num = 234;
		int rem = 0;
		int pro = 1;

		while(num>0){

			rem = num%10;
			num = num/10;
		
			pro = rem*pro;
		}
		System.out.println(pro);
	}
}
