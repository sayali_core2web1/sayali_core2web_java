class SumOfOddProOfEven{

	public static void main(String[] args){

		int num = 9367924;
		int rem = 0;
		int sum = 0;
		int pro = 1;

		while(num>0){

			rem = num%10;
			if(rem%2==0){

				pro = pro*rem;

			}else{

				sum = sum +rem;
			}
			num = num/10;
		}
		System.out.println("Sum of even digits :"+sum);
		System.out.println("Product of Odd digits :"+pro);
	}
}
