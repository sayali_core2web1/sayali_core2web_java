import java.util.*;

class AlphaNumeric{

	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);

		System.out.print("rows = ");

		int row = sc.nextInt();

		int num = row;

		int ch = 97+row;

		for(int i=1; i<=row; i++){

			for(int j=1; j<=row; j++){

				if(i%2==1){

					if(j%2==1){

						System.out.print(num*num-1 +" ");
					}else{

						System.out.print((char)ch +" ");
					}
				}else{

					System.out.print((char)ch +" ");
				}
				ch++;
				num++;
			}
			System.out.println();
		}
	}
}
