import java.util.*;

class SymbolNum{

	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);

		System.out.print("rows = ");

		int row = sc.nextInt();

		int num = row*row;

		for(int i=1; i<=row; i++){

			for(int j=1; j<=row; j++){

				if(i==j){

					System.out.print("$ ");
				}else{

					System.out.print(num*j +" ");

				}
				num--;
			}
			System.out.println();
		}
	}
}
