import java.util.*;

class AlphaNumeric{

	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);

		System.out.print("rows = ");

		int row = sc.nextInt();

		int ch = 96+row;

		int num = 1;

		int temp = row-1;

		for(int i=1; i<=row; i++){

			for(int j=1; j<=row; j++){

				if(i%2==1){

					if(j%2==0){

						System.out.print(num+temp +" ");
					}else{

						System.out.print((char)ch +" ");
					}
				
				}else{

					if(j%2==1){

						System.out.print(num+temp +" ");
					}else{

						System.out.print((char)ch +" ");
					}
				}
				ch++;
				num++;
			}
			System.out.println();
		}
	}
}
