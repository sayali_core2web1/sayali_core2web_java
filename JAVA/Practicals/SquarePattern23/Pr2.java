import java.util.*;

class AlphabetPatteren{

	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);

		System.out.print("rows = ");

		int row = sc.nextInt();

		int ch1 = 64+row;

		int ch2 = 96+row;

		int temp = row-1;

		for(int i=1; i<=row; i++){

			for(int j=1; j<=row; j++){

				if(temp>=j){

					System.out.print((char)ch2 +" ");
				}else{

					System.out.print((char)ch1 +" ");
				}
				ch1++;
				ch2++;
			
			}
			temp--;
			System.out.println();
		}
	}
}
