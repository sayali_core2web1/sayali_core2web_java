import java.util.*;

class SquareOrCol{

	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);

		System.out.print("rows = ");

		int row = sc.nextInt();

		int temp = 1;

		for(int i=1; i<=row; i++){

			int num = row;

			for(int j=1; j<=row; j++){

				if(i%2==1){

					if(j==1){

						temp = num*num;
						System.out.print(temp +" ");
						//temp--;
					}else{
					
						System.out.print(--temp +" ");
					}
				}else{

					temp = num*num;
					if(j==1){

						System.out.print(temp +" ");
					}else{
						temp = temp-5;
						if(j==2 || j==3){
							
							System.out.print(temp +" ");
			             		}else{

							System.out.print(temp-5 +" ");
						}
					}
				}
			}
			System.out.println();
		}
	}
}
